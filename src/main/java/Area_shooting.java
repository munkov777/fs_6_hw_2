import java.util.Scanner;

public class Area_shooting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[][] board = new char[5][5];
        int R = (int) (Math.random() * 5);
        int S = (int) (Math.random() * 5);

        System.out.println("All set. Get ready to rumble!");

        while (true) {
            System.out.println("Введіть рядок(1-5):");
            int r = scanner.nextInt() - 1;
            System.out.println("Введіть стовпець (1-5):");
            int s = scanner.nextInt() - 1;

            if (r < 0 || r > 4 || s < 0 || s > 4) {
                System.out.println("Упс, координати за межами поля бою");
            } else if (board[r][s] == '*' || board[r][s] == 'x') {
                System.out.println("Ви там вже стріляли!");
            } else if (r == R && s == S) {
                System.out.println("Вітаю, є влучання. Ви виграли");
                board[r][s] = 'x';
                printBoard(board);
                break;
            } else {
                System.out.println("Промах, спробуйте ще раз");
                board[r][s] = '*';
                printBoard(board);
            }
        }
    }

    public static void printBoard(char[][] board) {
        System.out.println("  | 1 | 2 | 3 | 4 | 5 |");
        System.out.println("-----------------------");
        for (int i = 0; i < board.length; i++) {
            System.out.print(i + 1 + " |");
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == 0) {
                    System.out.print(" - |");
                } else {
                    System.out.print(" " + board[i][j] + " |");
                }
            }
            System.out.println();
            System.out.println("----------------------");
        }
    }
}


